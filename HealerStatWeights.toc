## Interface: 80300
## Title: Healer Stat Weights
## Author: Bastas#6681 (discord)
## Version: 1.0.4
## Notes: Computes statweights for healers in real-time! This is the addon extension of the RDSW weakaura for resto druids, extended for all healing specs! Originally by Manaleaf; and updated by Voulk. We've recently updated the addon to account for corruption amplification effects with the help of Jundarer of the Dreamgrove.
## DefaultState: Enabled
## SavedVariables: HSW_DB 

embeds.xml

Classes/Util.lua

Classes/BuffTracker.lua
Classes/CastTracker.lua
Classes/Segment.lua
Classes/SegmentManager.lua
Classes/StatParser.lua
Classes/UnitManager.lua
Classes/Queues.lua
Classes/OptionsBuilder.lua

Parsers/Spells.lua
Parsers/HolyPriest.lua
Parsers/DiscPriest.lua
Parsers/RestoDruid.lua
Parsers/RestoShaman.lua
Parsers/HolyPaladin.lua
Parsers/MistweaverMonk.lua

DisplayPanel.lua
Core.lua
Events.lua